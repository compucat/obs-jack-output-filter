# obs-jack-output-filter
## *A little OBS filter to make any OBS source into a JACK source*
---

## Overview

This is a quick-n'-dirty thing that started with those fateful words, "I wonder how fast this could be implemented..." (answer: 5.5 hours from conception to initial PoC). It's been improved over a few days, but it is still far from release-ready; consider this a public beta until further tested.

The most common use case: suppose you are using OBS with an external audio mixer. All your audio sources are processed using said mixer, which sends its output into OBS for broadcast. This works great...until you try to add an OBS source that generates its own audio (i.e. a media source or NDI source): the mixer has no control over these sources' audio!

Using this plugin, audio from these sources can be sent to the mixer, consolidating all audio processing in one place. (I personally use this to process audio from remote RTMP streams through my Behringer X32.)

---

A couple of tiny things (chiefly, `plugin-macros.h.in` and portions of `CMakeLists.txt`) have been borrowed from https://github.com/Palaks/obs-tools (under GPLv2 license).

And yes, the source would be marginally simpler if I had stopped halfway through to rewrite the initial draft in C++...but I like my quadruple pointers :)

## Building

Ensure you have the build headers for JACK, OBS Studio, and pthread. Then build like any other CMake project (`mkdir build && cd build && cmake .. && make`) and copy the resulting `.so` file into your `obs-plugins` directory.

## Usage

Run JACK before starting OBS. Add the filter to any OBS source, and a new JACK source with the same name will automagically show up.

## Known quirks

- The JACK output taps audio before OBS' internal delay and processing, so it will often be heard out of phase (if not audibly before) the same source monitored through OBS itself.
- Buffer underruns happen every once in a while; the exact cause is not always known. Currently, this is mitigated by having a 32768-sample mutex-guarded ring buffer.
  - This does *not* delay all audio by 32768 samples; JACK will grab samples as fast as it can.
- Sample rates are assumed to be identical - this is not sanity-checked. Yet.
