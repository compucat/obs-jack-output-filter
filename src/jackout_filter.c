/*
jackout_filter: a lil' OBS filter that makes a source's audio available as a JACK audio device.
Initially prototyped 9/11/2020 in 5.5 hours by a very sleep-deprived Alex @CompuCat (https://compucat.me)
Licensed under GPLv2
*/

//TODO: investigate why direct JACK output can *sometimes* have a ton of latency compared to main output. On OBS startup, it's often earlier than main output. Reasons unknown.

#include <obs-module.h>
#include <jack/jack.h>
#include <stdio.h>
#include <pthread.h>
OBS_DECLARE_MODULE();
OBS_MODULE_USE_DEFAULT_LOCALE("jackout_filter", "en-US");
#define RINGBUF_SIZE 32768 //Size of each audio channel's internal ringbuffer, measured in samples (floats). TODO: dynamic this?

/*Internal data structures*/
struct jackout_channel //Represents a single JACK output channel and its associated ringbuffer.
{
  jack_port_t* output_port;
  float ringbuf[RINGBUF_SIZE];
  size_t first, last, buffill;
  pthread_mutex_t lock;
};
struct jackout_data //The context state of an instance of the filter, passed around between callbacks.
{
  obs_source_t *context;
  size_t numChannels;
  jack_client_t *client;
  char *jack_client_name;
  struct jackout_channel **channels; //Dynamic array of pointers to jackout_channel structs. Yes, this is pointer madness.
  bool jack_enabled, jack_exclusive;
};

/*JACK functions*/
int processJack(jack_nframes_t nframes, void *arg)
{
    struct jackout_data *ctx = arg;
    /*Copy audio from internal FIFO to JACK output buffer.*/
    for(size_t c=0; c<ctx->numChannels; c++)
    {
      struct jackout_channel *chan = ctx->channels[c];
      pthread_mutex_lock(&(chan->lock));
      jack_default_audio_sample_t *out = jack_port_get_buffer(chan->output_port, nframes);
      for(size_t i=0; i<nframes; i++) //TODO: memcpy is faster but incompatible with ringbufs. Better way to do this?
      {
        if(ctx->jack_enabled && chan->buffill>0)
        {
            *(out++)=chan->ringbuf[chan->first];
            chan->buffill--;
            chan->first=(chan->first+1)%RINGBUF_SIZE;
        }
        else *(out++)=0; //zero on underruns. the alternative is to loop...TODO make this an option?
      }
      pthread_mutex_unlock(&(chan->lock));
    }
    return 0;
}

void jackout_init_jack(struct jackout_data *ctx, const char* name) //Initialize a JACK client
{
  /*Open the JACK client itself*/
  ctx->jack_client_name = bzalloc(jack_port_name_size()); //TODO: guard these strcats more carefully to avoid shenanigans
  strcat(ctx->jack_client_name, "OBS ");
  strcat(ctx->jack_client_name, name);
  jack_status_t status;
  ctx->client = jack_client_open(ctx->jack_client_name, JackNullOption, &status, NULL);
  if (ctx->client == NULL) {blog(100, "ERROR: JACK client is NULL! Brace for impact.\n");} //TODO: actual error handling if JACK client fails to open
  if (status & JackNameNotUnique) ctx->jack_client_name = jack_get_client_name(ctx->client);
  jack_set_process_callback(ctx->client, processJack, ctx);
  //TODO: add a sane callback for jack_on_shutdown
  //TODO: sanitycheck that samplerates are matched. (the actual audio formats themselves should be guaranteed floats)

  /*Allocate structs for each audio channel and create their JACK outputs.*/
  ctx->channels = bzalloc(ctx->numChannels * sizeof(*(ctx->channels))); //Allocate the dynamic array of pointers
  for (int i=0; i<ctx->numChannels; i++)
  {
    ctx->channels[i] = bzalloc(sizeof(*(ctx->channels[i])));
    struct jackout_channel *chan = ctx->channels[i];
    if(pthread_mutex_init(&(chan->lock), NULL)) blog(100, "jackout_init_jack(): pthread mutex init failed! Brace for impact.");

    pthread_mutex_lock(&(chan->lock));
    if(chan == NULL) blog(100, "jackout_init_jack(): jackout_channel %d pointer is NULL! Brace for impact.", (int)i);
    /*Set up the ringbuffer*/
    //No need to zero-fill the ringbuffer, thanks bzalloc()
    chan->first=0;
    chan->last=0;
    chan->buffill = 0;
    /*Set up a JACK port*/
    char port_name[jack_port_name_size()];
    sprintf(port_name, "port_%d", i);
    chan->output_port = jack_port_register (ctx->client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    pthread_mutex_unlock(&(chan->lock));
  }
  if(jack_activate(ctx->client)) {blog(100, "ERROR: jack_activate() returned nonzero\n");} //TODO: error handle this
}

void jackout_deinit_jack(struct jackout_data *ctx) //We need to be *very* careful about memory cleanup here :)
{
  jack_client_close(ctx->client);
  ctx->client = NULL;
  bfree(ctx->jack_client_name);
  for(int i=0; i<ctx->numChannels; i++) bfree(ctx->channels[i]);
  bfree(ctx->channels);
}

/*OBS plugin callbacks*/
const char *jackout_name(void *unused) {return obs_module_text("JACK Output");}

void jackout_update(void *data, obs_data_t *s)
{
  struct jackout_data *ctx = data;
  ctx->jack_enabled = obs_data_get_bool(s, "jack_enabled");
  ctx->jack_exclusive = obs_data_get_bool(s, "jack_exclusive");
}

void *jackout_create(obs_data_t *s, obs_source_t *filter)
{
    /*Initialize internal context and make OBS happy*/
    struct jackout_data *ctx = bzalloc(sizeof(*ctx));
    ctx->context = filter;
    ctx->numChannels = audio_output_get_channels(obs_get_audio()); //Copied from canonical implementation. TODO: is there a way to get the channel count of the source and not the global count? Maybe analyze the actual audio data struct?

    jackout_update(ctx, s);

    //It might look like we forgot to initialize JACK! This is intentional.
    //JACK only lets you set the client's name on initialization, and we need to call obs_filter_get_parent() to do that.
    //That function is only guaranteed to be valid inside a few callbacks - notably, filter_audio: thus, we do it there.

    return ctx;
}

void jackout_destroy(void *data)
{
    struct jackout_data *ctx = data;
    if(ctx->client) jackout_deinit_jack(ctx);
    bfree(ctx);
}

struct obs_audio_data *jackout_filter_audio(void *data, struct obs_audio_data *audio)
{
    struct jackout_data *ctx = data;

    /*This is BAD: initializing JACK and doing lots of mallocs in a realtime audio callback.
      Justification: only doing this on the very first callback.
      Required so that getting the name of the parent source is guaranteed since this isn't changeable after JACK client init.
    */
    obs_source_t *parent = obs_filter_get_parent(ctx->context);
    if(!(ctx->client)) jackout_init_jack(ctx, (parent ? obs_source_get_name(parent) : "JACK Output")); //Sanity check just in case.

    /*Copy OBS audio data into FIFO.*/
    float **adata = (float**) audio->data;
    for (size_t c=0; c<ctx->numChannels; c++) if(audio->data[c])
    {
      struct jackout_channel *chan = ctx->channels[c];
      pthread_mutex_lock(&(chan->lock));
      for(size_t i=0; i<audio->frames; i++) //TODO: memcpy is faster but incompatible with ringbufs. Better way to do this?
      {
        chan->ringbuf[chan->last]=adata[c][i];
        chan->last=(chan->last+1)%RINGBUF_SIZE;
        if(ctx->jack_enabled && ctx->jack_exclusive) adata[c][i]=0; //If JACK exclusive output is enabled, mute audio going to OBS' mixer.
      }
      chan->buffill+=audio->frames;
      pthread_mutex_unlock(&(chan->lock));
    }

    return audio;
}

void jackout_defaults(obs_data_t *data)
{
  obs_data_set_default_bool(data, "jack_enabled", true);
  obs_data_set_default_bool(data, "jack_exclusive", false);
}

obs_properties_t* jackout_properties(void *data)
{
  obs_properties_t *prop = obs_properties_create();
  obs_property_t *p;

	obs_properties_add_bool(prop, "jack_enabled", obs_module_text("Enable JACK output"));
  obs_properties_add_bool(prop, "jack_exclusive", obs_module_text("Disable OBS mixer output when using JACK"));

  return prop;
}

struct obs_source_info jackout_source =
{
    .id = "jackout_filter",
    .type = OBS_SOURCE_TYPE_FILTER,
    .output_flags = OBS_SOURCE_AUDIO,
    .get_name = jackout_name,
    .create = jackout_create,
    .destroy = jackout_destroy,
    .update = jackout_update,
    .filter_audio = jackout_filter_audio,
    .get_defaults = jackout_defaults,
    .get_properties = jackout_properties,
};

bool obs_module_load()
{
    obs_register_source(&jackout_source);
    return true;
}
